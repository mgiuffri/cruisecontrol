library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;

entity AcceleratorPriority is
	port ( is_cruise_active: std_logic;
		driverAcceleratorIn, pidAcceleratorIn : in std_logic_vector(15 downto 0);
		accelatorOutput   : out std_logic_vector(15 downto 0));
end AcceleratorPriority;

architecture arch of AcceleratorPriority is
 signal shouldSelectPid: std_logic;
 component Mux_2_1_Nbit is
	generic(N : integer);
	port( 
			s: in std_logic;
			a, b: in std_logic_vector(N - 1 downto 0);
			O: out std_logic_vector(N - 1 downto 0));
end component;
begin

	shouldSelectPid <= is_cruise_active and (not or_reduce(driverAcceleratorIn));
	mux: Mux_2_1_Nbit 
		generic map(N => 16)
		port map (shouldSelectPid, driverAcceleratorIn, pidAcceleratorIn, accelatorOutput);

end arch;
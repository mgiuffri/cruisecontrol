library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_signed.all;
use ieee.std_logic_arith.all;


-- Adder_8bit_carry
-- Simple 8 bit adder with out carry.
entity Adder_8bit_carry is
	port( 
			a, b: in std_logic_vector(7 downto 0);
			sum: out std_logic_vector(7 downto 0);
			cout: out std_logic);
end Adder_8bit_carry;

architecture arch of Adder_8bit_carry is
	signal padded_a, padded_b, temp_sum: std_logic_vector(8 downto 0);
begin
 padded_a <= '0' & a;
 padded_b <= '0' & b;
 
 temp_sum <= padded_a + padded_b;
 
 sum <= temp_sum(7 downto 0);
 cout <= temp_sum(8);
end arch;
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity ClockFrequencyDivider is
  generic(FACTOR : integer);
  port (in_clk : in  std_logic;
		reset : in  std_logic;
		out_clk   : out std_logic);
end ClockFrequencyDivider;

architecture arch of ClockFrequencyDivider is
  signal counter : integer range 0 to FACTOR - 1 := 0;
  signal temp_out: std_logic;
begin

  frequency_divider: process (in_clk, reset)
  begin
    if reset = '1' then        
      counter <= 0;
		temp_out <= '0';
    elsif rising_edge(in_clk) then
		if (counter = FACTOR - 1) then
                temp_out <= NOT(temp_out);
                counter <= 0;
            else
                counter <= counter + 1;
            end if;
    end if;
  end process;
  
  out_clk <= temp_out;

end arch;
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;

entity CruiseControl is
	port( clk, on_off, res_coast, plus_speed, minus_speed, brake: in std_logic;
			driverAcceleratorInput: std_logic_vector(15 downto 0);
			car_speed: in std_logic_vector(7 downto 0);
			cruise_control_state: out std_logic_vector(1 downto 0);
			selected_speed: out std_logic_vector(7 downto 0);
			pid_output: out std_logic_vector(15 downto 0);
			acceleratorOutput: out std_logic_vector(15 downto 0));
end CruiseControl;

architecture arch of CruiseControl is

component StateMachine is
	port(
		on_off, res_coast, is_working_speed, set_speed, brake, CLK: in std_logic;
		cruise_out: out std_logic_vector(1 downto 0));
end component;

component SpeedChecker is
	port( car_speed: in std_logic_vector(7 downto 0);
			speed_in_range : out std_logic);
end component;

component PidController is
	generic(InputSize, OutputSize,  Kp, Ki, Kd: integer); 
	port ( 
			  clk, reset: in Std_logic; 	
			  set_point, input : in  STD_LOGIC_VECTOR (InputSize - 1 downto 0); 
           controllerOutput : out  STD_LOGIC_VECTOR (OutputSize - 1 downto 0));
end component;

component SpeedSelector is
	port(
		clk, reset, plus_speed, minus_speed, is_working_speed, is_cruise_active: in std_logic;
		car_Speed: in std_logic_vector(7 downto 0);
		selected_speed: out std_logic_vector(7 downto 0));
end component;


component AcceleratorPriority is
	port ( is_cruise_active: std_logic;
		driverAcceleratorIn, pidAcceleratorIn : in std_logic_vector(15 downto 0);
		accelatorOutput   : out std_logic_vector(15 downto 0));
end component;

signal is_working_speed, virtual_set_button, reset, is_cruise_active: std_logic;
signal temp_state_out: std_logic_vector(1 downto 0);
signal temp_selected_speed: std_logic_vector(7 downto 0);
signal temp_pid_out: std_logic_vector(15 downto 0);
begin

 pid: PidController
	generic map (8, 16, 800, 40, 20)
	port map (clk, is_cruise_active, temp_selected_speed, car_speed, temp_pid_out);
		
 speed_validator: SpeedChecker port map (car_speed, is_working_speed);
 
 state_machine: StateMachine port map(
			on_off, res_coast, is_working_speed, virtual_set_button, brake, CLK, temp_state_out);
 
 speedComponent: SpeedSelector port map(
			clk, reset, plus_speed, minus_speed, is_working_speed, is_cruise_active, car_Speed, temp_selected_speed);
	
 accel: AcceleratorPriority port map (
			is_cruise_active, driverAcceleratorInput, temp_pid_out, acceleratorOutput);
	
 reset <= not on_off;
 virtual_set_button <= plus_speed or minus_speed;
 is_cruise_active <= and_reduce(temp_state_out);
 cruise_control_state <= temp_state_out;
 selected_speed <= temp_selected_speed;
 pid_output <= temp_pid_out;
 
end arch;
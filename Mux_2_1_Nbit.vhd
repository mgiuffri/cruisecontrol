library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;


-- Mux_2_1_Nbit
-- A 1-bit selector value determines which of 2 N-bit values
-- should be sent in output.
entity Mux_2_1_Nbit is
	generic (N: integer);
	port( 
			s: in std_logic;
			a, b: in std_logic_vector(N -1 downto 0);
			O: out std_logic_vector(N -1 downto 0));
end Mux_2_1_Nbit;

architecture arch of Mux_2_1_Nbit is
begin
 O <= a when s='0' else
		b when s='1';
end arch;
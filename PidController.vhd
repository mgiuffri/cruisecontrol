library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;

entity PidController is
	-- PID coefficents
	-- Kp proportional factor
	-- Ki integral factor
	-- Kd differential factor
	generic(InputSize, OutputSize,  Kp, Ki, Kd : integer); 
	port ( 
			  clk, reset: in std_logic; 	
			  set_point, input : in  std_logic_vector (InputSize - 1 downto 0); 
           controllerOutput : out  std_logic_vector (OutputSize - 1 downto 0));
end PidController;


architecture arch of PidController is	
subtype positiveInteger is integer range 0 to (2**InputSize - 1);
subtype errorInteger is integer range - (2**InputSize - 1) to (2**InputSize - 1);
begin
process(clk, reset)
	variable error :  errorInteger :=0;
	variable previousError : errorInteger :=0 ;
	variable errorSum:  errorInteger := 0;
	variable errorChange:  errorInteger := 0;
	variable output:  integer:=0;
	variable setpoint: positiveInteger :=0;
	variable inp: positiveInteger :=0;
	variable p,i,d : integer;
	constant max_output : integer := (2**OutputSize) - 1;
begin	 

	if(reset = '0') then
			error:=0;
			previousError:=0;
			errorSum:=0;
			errorChange:=0;
			output:=0;
	elsif (rising_edge(clk)) then 
		inp := to_integer(unsigned(input));
		setpoint := to_integer(unsigned(set_point));	
		error := (setpoint - inp);
		errorSum	:= errorSum + error;
		errorChange := error - previousError;
		p := Kp * error;
		i := Ki * errorSum;
		d:= Kd * errorChange;
		output := (p + i + d);
		previousError := error;
		if(output > max_output) then
			output := max_output;
		elsif(output < 0) then
			output := 0;
		end if;
	end if;
	controllerOutput <= std_logic_vector(to_unsigned(output, OutputSize));
end process;
end arch;
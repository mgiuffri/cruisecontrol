# CRUISE CONTROL
di Mariano Giuffrida

Progetto finale -  Reti Logiche - Politecnico di Milano
Prof. Bruschi

La specifica di Progetto può essere trovata [qui](https://bitbucket.org/mgiuffri/cruisecontrol/src/master/Specifiche%20Progetto.pdf).

Il progetto è stato realizzato in linguaggio VHDL usando il software Quartus Prime Lite edition (v18).

La funzionalità del sistema è stata verificata tramite simulazione software (file in *.vwf).
 
Gli scenari di test sono spiegati nel file: [Sims explanations](https://bitbucket.org/mgiuffri/cruisecontrol/src/master/Sims%20Explanations.txt)


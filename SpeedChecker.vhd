library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;

-- Check if speed is above 30km/h and below 200km/h
entity SpeedChecker is
	port( car_speed: in std_logic_vector(7 downto 0);
			speed_in_range : out std_logic);
end SpeedChecker;

architecture arch of SpeedChecker is
	signal above30k, above200k: std_logic;
begin
-- this could be esily calculated by interpreting car_speed as an integer
-- to_integer(unsigned(car_speed)) and then using simple comparing logic but was exercising using bits.
 above30k <= (or_reduce(car_speed(7 downto 5)) OR and_reduce(car_speed(4 downto 1)));
 above200k <= (car_speed(7) AND car_speed(6) AND (or_reduce(car_speed(5 downto 3))) AND (or_reduce(car_speed(2 downto 0))));
 speed_in_range <= above30k and not above200k;
end arch;
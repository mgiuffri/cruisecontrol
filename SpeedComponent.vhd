library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
-- Speed Component
-- This registers a selected speed as well as allows to increase 
-- and decrease the selected speed a unit at a time.
entity SpeedSelector is
	port(
		clk, reset, plus_speed, minus_speed, is_working_speed, is_cruise_active: in std_logic;
		car_Speed: in std_logic_vector(7 downto 0);
		selected_speed: out std_logic_vector(7 downto 0));
		
end SpeedSelector;

architecture arch of SpeedSelector is

component Reg_PP_8_bit is
	port( 
			clk: in std_logic;
			reset: in std_logic;
			I: in std_logic_vector(7 downto 0);
			O : out std_logic_vector(7 downto 0));
end component;

component Mux_2_1_Nbit is
	generic(N : integer);
	port( 
			s: in std_logic;
			a, b: in std_logic_vector(N - 1 downto 0);
			O: out std_logic_vector(N - 1 downto 0));
end component;

component Adder_8bit_carry is
	port( 
			a, b: in std_logic_vector(7 downto 0);
			sum: out std_logic_vector(7 downto 0);
			cout: out std_logic);
end component;

component ClockFrequencyDivider is
  generic(FACTOR : integer);
  port (in_clk, reset : in  std_logic;
			out_clk   : out std_logic);
end component;

signal reg_in,adder_out,reg_out,enable_mux_out,updown_mux_out : std_logic_vector (7 downto 0);
signal should_add, scaled_clock, set_speed, virtual_set_button: std_logic;
-- i seguenti segnali svolgono la funzione di costanti
constant zero : std_logic_vector(7 downto 0):= (others => '0');
constant one : std_logic_vector(7 downto 0):= "00000001";
constant minus_one : std_logic_vector(7 downto 0):= (others => '1');
begin
	-- Selected Speed can be set only if speed is within working range and cruise control is not active
	-- when active, plus and minus button increase and decrease the selected speed.
	virtual_set_button <= (plus_speed or minus_speed);
	set_speed <=  virtual_set_button and is_working_speed and not is_cruise_active;
 
	
	-- To find the factor calculate the ( input frequency/Desired Frequency )
	-- This number should be divided by 2 (cos of 50% duty cycle)
	-- For real life scenario, the desired sampling frequency would have to take into account 
	-- average user click speed. For the purpose of simulation, we are using a factor of 1 - 
	-- which halfs the input clock.
	clock_scaler: ClockFrequencyDivider
		generic map (Factor => 1)
		port map (clk, '0', scaled_clock);

	speed_register: Reg_PP_8_bit port map (scaled_clock ,reset, reg_in, reg_out);
	
	set_speed_mux: Mux_2_1_Nbit 
		generic map(N => 8)
		port map (set_speed, adder_out, car_speed, reg_in);
		
	adder: Adder_8bit_carry port map (reg_out, enable_mux_out, adder_out);
	
	-- the enable_mux and updown_mux in conjuction determine whether the adder should be 
	-- adding a zero, +1 or -1
	-- enable_mux is driven high only if wither then plus or minus button are selected;
	-- when high, the out of the updown mux is sent to out pin;
	-- The updown mux is driven simply by the minus_speed signal;
	should_add <= (plus_speed xor minus_speed) and is_cruise_active;	
	
	enable_mux: Mux_2_1_Nbit
		generic map(N => 8)
		port map (should_add, zero, updown_mux_out, enable_mux_out);
		
	updown_mux: Mux_2_1_Nbit 
		generic map (N => 8)
		port map (minus_speed, one, minus_one, updown_mux_out);
		
	selected_speed <= reg_out;
end arch;

library ieee;
use ieee.std_logic_1164.all;

entity StateMachine is
	port(
		on_off, res_coast, is_working_speed, set_speed, brake, CLK: in std_logic;
		cruise_out: out std_logic_vector(1 downto 0)
		);
end StateMachine;

architecture moore_State_machine of StateMachine is
	type STATUS is (OFF, CRUISE_SBY, CRUISE_COAST, CRUISE_ACTIVE);
	signal present_state, next_state: STATUS;
	signal temp_output: std_logic_vector(1 downto 0);
begin

delta: process(on_off, brake, is_working_speed, set_speed, res_coast)
begin
	case present_state is
		when OFF =>
			if(on_off='1') then
				next_state <= CRUISE_SBY;
			else
				next_state <= OFF;
			end if;
		when CRUISE_SBY =>
			if(on_off='0') then
				next_state <= OFF;
			elsif(set_speed = '1' and is_working_speed = '1') then
				next_state <= CRUISE_ACTIVE;
			else
				next_state <= CRUISE_SBY;
			end if;
		when CRUISE_COAST =>
			if(on_off='0') then
				next_state <= OFF;
			elsif((res_coast = '1' or set_speed = '1') and is_working_speed='1') then
				next_state <= CRUISE_ACTIVE;
			else
				next_state <= CRUISE_COAST;
			end if;
		when CRUISE_ACTIVE =>
			if(on_off='0') then
				next_state <= OFF;
			elsif(brake = '1' or is_working_speed = '0' or res_coast = '1') then
				next_state <= CRUISE_COAST;
			else
				next_state <= CRUISE_ACTIVE;
			end if;
	end case;
end process;	

with present_state select 
	temp_output <=
		"00" when OFF,
		"01" when CRUISE_SBY,
		"10" when CRUISE_COAST,
		"11" when CRUISE_ACTIVE;
	
state_output: process(CLK)
begin
	if(rising_edge(CLK)) then
		present_state <= next_state;
		cruise_out <= temp_output;
	end if;
end process;

end moore_State_machine;
	
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;


-- Simple Synchronous 8 bit Register
-- with async reset.
entity Reg_PP_8_bit is
	port( 
			clk: in std_logic;
			reset: in std_logic;
			I: in std_logic_vector(7 downto 0);
			O : out std_logic_vector(7 downto 0));
end Reg_PP_8_bit;

architecture arch of Reg_PP_8_bit is
begin

 rec: process(reset, clk)
 begin
	if(reset = '1') then
			O <= "00000000";
	elsif(clk'event and clk = '1') then
		O <= I;
	end if;
 end process;
 
end arch;